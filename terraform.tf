terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=2.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "example-rg"
  location = "East US"
}

resource "azurerm_key_vault" "example" {
  name                        = "example-keyvault"
  resource_group_name         = azurerm_resource_group.example.name
  location                    = azurerm_resource_group.example.location
  sku_name                    = "standard"
  tenant_id                   = "<Your Azure Tenant ID>"
  soft_delete_enabled         = true
  purge_protection_enabled    = false
  enable_rbac_authorization   = false
  enable_soft_delete_purge_protection = true

  access_policy {
    tenant_id = "<Your Azure Tenant ID>"
    object_id = "<Object ID of the user, group, or application>"
    secret_permissions = [
      "get",
      "list",
    ]
  }
}

output "key_vault_url" {
  value = azurerm_key_vault.example.vault_uri
}

